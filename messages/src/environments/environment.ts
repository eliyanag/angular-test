// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

//משתנה קבוע שהאפליקציה לא יכולה לשנות את הערכים שלו, משמש למטרה שהאפליקציה אמורה לעבוד בסביבות שונות
//בסביבת פיתוח נעבור מול השרת (סלים) בשונה מסביבת טסט שעובדת מקומית
//למשל כדי לדעת להבדיל בין הסביבות נשים אייקון של טסט, בעזרת המשתנה הנ"ל אנו נוכל להגדיר אם הוא יופיע או לא

//הגדרה מקומית localhost
export const environment = {
  production: false,
  url: 'http://localhost/angular/slim/',
  firebase:{
    apiKey: "AIzaSyBwulJH0RXS5XpQXWTnCoYgJbvw9gD43V4",
    authDomain: "messages-a47e5.firebaseapp.com",
    databaseURL: "https://messages-a47e5.firebaseio.com",
    projectId: "messages-a47e5",
    storageBucket: "messages-a47e5.appspot.com",
    messagingSenderId: "832647356700"
  }
};